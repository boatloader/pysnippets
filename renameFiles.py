outputFiles = ['abs.004', 'abs.002', 'abs.001', 'abs.003']
fileExtensionPrefix = "ext"

sortedOutputFiles = sorted(outputFiles)
print("sortedOutputFiles: ", sortedOutputFiles)

for idx, item in enumerate(sortedOutputFiles, start = 0):
    splitString = list(item.rpartition('.'))
#    print("splitString before: ", idx, " is ", splitString)
    splitString[2] = fileExtensionPrefix + str(idx)
#    print("splitString after: ", idx, " is ", splitString)
    sortedOutputFiles[idx] = ''.join(splitString)
#    print("sortedOutputFiles[",idx,"]: ", sortedOutputFiles[idx])

print("sortedOutputFiles: ", sortedOutputFiles)